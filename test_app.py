from flask import Flask
import app
import unittest
import requests

app=Flask(__name__)

class Hell_test(unittest.TestCase):
    
    def test_add(self):
        x = requests.get('http://13.209.15.210:8062/add', params = {'a':2, 'b':3})
        result=x.text
        self.assertEqual(result,str(5))

    def test_sub(self):
        x = requests.get('http://13.209.15.210:8062/sub', params = {'a':2, 'b':3})
        result= x.text
        self.assertEqual(result,str(-1))

if __name__=="__main__":
    unittest.main()


#######